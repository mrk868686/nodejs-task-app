const sgMail = require('@sendgrid/mail');

sgMail.setApiKey(process.env.SENDGRID_API_KEY);

const sendWelcomeEmail = (email, name) => {
  sgMail.send({
    to: email,
    from: 'kisrezsopkr@gmail.com',
    subject: 'Thanks for joining in!',
    text: `Welcome to the app, ${name}. Let me know you get along with the app.`,
  });
};

const sendCancelationEmail = (email, name) => {
  sgMail.send({
    to: email,
    from: 'kisrezsopkr@gmail.com',
    subject: 'Sorry to hear leaving!',
    text: `Thank you for using this app, ${name}. Your cancelation were successful. We are welcome you to back anytime!`,
  });
};

module.exports = {
  sendWelcomeEmail,
  sendCancelationEmail,
};
